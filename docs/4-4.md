# Activity: Using the Well-Architected Framework 

![KSB Logo](img/KSB_logo.svg)

[TOC]

---

## Design Principles and Patterns for Data Pipelines Activity: Using the Well-Architected Framework

## Situation

[Data Analytics Lens-Website](https://docs.aws.amazon.com/wellarchitected/latest/analytics-lens/well-architected-design-principles.html)

Sie wurden gebeten, Cloud-Designprinzipien und die damit verbundenen Best Practices zu identifizieren, die Ihr Data-Engineering-Team beim Aufbau einer Datenpipelines anwenden sollte.

1. Verwenden Sie die [Data Analytics Lens-Website](https://docs.aws.amazon.com/wellarchitected/latest/analytics-lens/well-architected-design-principles.html)  als Ihre Hauptquelle. Möglicherweise finden Sie auch relevante bewährte Methoden auf der Hauptwebsite des [AWS Well-Architected Framework](https://docs.aws.amazon.com/wellarchitected/latest/framework/the-pillars-of-the-framework.html).
2. Prüfen Sie im Data Analytics Lens die Entwurfsprinzipien und die damit verbundenen Best Practices in jeder der fünf Säulen (Operational Excellence, Security, Reliability, Performance Efficiency, Cost Optimization und Sustainability). Finden Sie für jede Säule eine bewährte Methode, die sich auf eines der fünf Daten-Vs (variety, volume, velocity, veracity, and value) bezieht, die im Modul "The Elements of Data" eingeführt wurden.
3. Erfassen Sie für jede Säule die nötigen Informationen zu den Punkte a-c und tauschen Sie sich mit ihren Kolleginnen und Kollegen aus, indem Sie die von Ihrem Lehrer festgelegte Methode anschauen.
    a. Das Design-Prinzip und die Best-Practice dazu
    b. Ein oder mehrere Merkmale in Bezug auf die 5 Vs, auf die sich Ihrer Meinung nach das gewählte Prinzip beziehen
    c. Die URL der Seite angeben, auf der sich die Infos für die  verwendeten Methode, mit der Sie die entsprechende Säule identifizieren konnten, befinden

## Lösungsbeispiel

Für den Thread „Operational Excellence“ könnten Sie mit den folgenden Informationen arbeiten:

- a.  Datenprinzip 1 — Überwachen Sie den Zustand der Analysepipelines, Best Practice 1.1 — Überprüfen Sie die Datenqualität der Quellsysteme, bevor Sie zur Analytik übergehen
- b.  Diese  Methode bezieht sich auf die Korrektheit der Daten (variety).
- c. [https://docs.aws.amazon.com/wellarchitected/latest/analytics-lens/best-practice-1-1.html](https://docs.aws.amazon.com/wellarchitected/latest/analytics-lens/best-practice-1-1.html)

## Task/Auftrag bis Dienstag 11. Juni 2025

Erstellen Sie ein wohl formatiertes MD-Dokument. überführen Sie das MD-Dokument in ein PDF. Geben Sie beide Dokumente via TEAMS-Assignment rechtzeitig ab.
Wählen Sie eine Säule und erstellen Sie nach dem Muster im Lösungsbeispiel ihre Lösung. Geben Sie sich bei der Gestaltung des MD-Dokumentes Mühe. Arbeiten Sie mit Links, Tabellen und aussagekräftigen Bildern. Wohl formatiert meint: Name, Datum, Seitennummer, sinnvolle Struktur
Viel Erfolg: Note zählt zum LBV Bereich Activities.

---
&copy;KSB, 2024, Modul 106, Datenbanken abfragen, bearbeiten und warten