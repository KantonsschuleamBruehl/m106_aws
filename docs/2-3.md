## Data driven Organizations

In diesem Modul wurde die datengesteuerte Entscheidungsfindung vorgestellt und wie Datenanalyse und KI/ML die Entscheidungsfindung unterstützen können. Sie haben gelernt, dass sowohl Datenanalyse als auch KI/ML bei Vorhersagen helfen können, aber KI/ML tut dies, indem sie über die Daten lernt, anstatt programmierte Regeln zu verwenden. 

Sie lernten die vier Hauptebenen einer Datenpipeline kennen: **Aufnahme**, **Speicherung**, **Verarbeitung **sowie **Analyse und Visualisierung. **

Sie wurden auch mit der Art der Aktionen vertraut gemacht, die an den Daten vorgenommen werden, während sie die Pipeline durchlaufen, z. B. Bereinigung und Transformation. Sie erfuhren, welche Bedenken ein Dateningenieur oder Datenwissenschaftler beim Aufbau und der Verwendung einer Datenpipeline haben kann. 

Und schließlich lernten Sie den Ansatz „<mark>Modernisieren, Vereinheitlichen und Erneuern</mark>“ für den Aufbau moderner Dateninfrastrukturen kennen.
