# Querying Data by Using Athena 

![KSB Logo](img/KSB_logo.svg)

[TOC]

---

## Lab overview and objectives

Sofía ist mit dem Proof of Concept (POC) zufrieden, den Sie für Amazon Simple Storage Service (Amazon S3) Select erstellt haben, und das Team lernt, ihn zu verwenden. Eine der Datenwissenschaftlerinnen, Mary, möchte jedoch komplexere Abfragen für Daten mit komma-getrennten Werten (CSV) durchführen. Sie arbeitet oft mit grossen Datensätzen, die in mehreren CSV-Dateien gespeichert sind, und sie kann S3 Select nicht mit mehreren Dateien verwenden. Daher hätte sie gerne die Möglichkeit, mehrere Dateien im selben S3-Bucket abzufragen und die Daten aus diesen Dateien in derselben Datenbank zu aggregieren. Sie möchte auch in der Lage sein, die Datensatz-Metadaten wie Spaltennamen und Datentypdeklarationen zu transformieren. Diese Informationen sind Teil des Datensatzschemas, wenn die Daten als relationale Datenbank gespeichert werden.

Durch einige Entdeckungen erfahren Sie, dass Amazon Athena diese Funktionalität bietet. Sie erkunden Athena und seine Funktionen, um herauszufinden, ob Sie Marys Bedürfnisse erfüllen können. Athena ist ein interaktiver Abfrageservice, mit dem Sie Daten abfragen können, die in Amazon S3 gespeichert sind. Athena speichert Daten über die Datenquellen, die Sie abfragen. Sie können Ihre Abfragen zur Wiederverwendung speichern und sie mit anderen Benutzern teilen.

In diesem Lab lernen Sie, wie Sie Athena und AWS Glue verwenden, um Daten abzufragen, die in Amazon S3 gespeichert sind.

Nach Abschluss dieses Labors sollten Sie in der Lage sein, Folgendes zu tun:

- Verwenden Sie den Athena-Abfrage-Editor, um eine AWS Glue-Datenbank und -Tabelle zu erstellen.
- Definieren Sie das Schema für die AWS Glue-Datenbank und die zugehörigen Tabellen mithilfe der Athena-Funktion zum Massenhinzufügen von Spalten.
- Konfigurieren Sie Athena so, dass es einen Datensatz verwendet, der sich in Amazon S3 befindet.
- Optimieren Sie Athena-Abfragen anhand eines Beispieldatensatzes.
- Erstellen Sie Ansichten in Athena, um die Datenanalyse für andere Benutzer zu vereinfachen.
- Erstellen Sie benannte Athena-Abfragen mithilfe von AWS CloudFormation.
- Prüfen Sie eine AWS-Richtlinie für Identity and Access Management (IAM), die Benutzern zugewiesen werden kann, die benannte Athena-Abfragen verwenden möchten.
- Vergewissern Sie sich, dass ein Benutzer mithilfe der AWS-Befehlszeilenschnittstelle (AWS CLI) im AWS Cloud9-Terminal auf eine benannte Athena-Abfrage zugreifen kann.

## Duration

This lab will require approximately 90 minutes to complete.

# AWS service restrictions

In dieser Laborumgebung ist der Zugriff auf AWS-Services und Serviceaktionen möglicherweise auf diejenigen beschränkt, die zur Ausführung der Laboranweisungen erforderlich sind. Möglicherweise treten Fehler auf, wenn Sie versuchen, auf andere Services zuzugreifen oder Aktionen auszuführen, die über die in diesem Lab beschriebenen hinausgehen.

## Scenario

In diesem Lab übernehmen Sie die Rolle eines Mitglieds des Data-Science-Teams. Sie erstellen einen POC mit AWS Glue und Athena, um die in Amazon S3 gespeicherten Daten zu analysieren. Sie möchten mit Athena experimentieren, indem Sie auf Rohdaten in einem S3-Bucket zugreifen, eine AWS Glue-Datenbank erstellen und diese Datenbank mithilfe von Athena abfragen.

Sie möchten auch herausfinden, ob Sie diese Lösung so skalieren können, dass andere Teammitglieder Zugriff darauf haben. Mary ist Teil der IAM-Gruppe in AWS für das Data-Science-Team und hat ähnlichen Zugriff auf den S3-Bucket, die AWS Glue-Datenbank und Athena. Sie haben ihren Zugriff jedoch eingeschränkt, um eine unnötige Erstellung von Ressourcen zu verhindern. Dies folgt dem Prinzip der geringsten Rechte, bei dem ein Administrator den Zugriff auf AWS-Services und -Ressourcen auf der Grundlage der für die Ausführung einer bestimmten Aufgabe oder Aufgabe erforderlichen Berechtigungen einschränkt. In einer Aufgabe testen Sie mit einem IAM-Benutzer, ob ein Mitglied des Data-Science-Teams in der Lage ist, von Athena verwaltete Abfragen auszuführen. Der IAM-Benutzer wurde für Sie erstellt und der Benutzer gehört zu einer IAM-Gruppe, der eine Richtlinie zur Definition von Berechtigungen zugeordnet ist.

Sie fragen Mary, ob sie einige Beispieldatensätze hat, die Sie für Ihre Experimente verwenden können. Mary bietet Zugriff auf einen Datensatz, der Taxifahrtendaten für 2017 enthält. Sie werden die Daten für Ihren POC verwenden.

Das folgende Diagramm veranschaulicht die Umgebung, die zu Beginn des Labors in AWS für Sie erstellt wurde.

![athena_lab_environment](img/m4_a_01_athena_lab_environment.png)

!!! Tipp

    Um die CloudFormation-Vorlage zu überprüfen, mit der diese Umgebung erstellt wurde, navigieren Sie nach der Anmeldung bei der AWS Management Console zur CloudFormation-Konsole. Wählen Sie im Navigationsbereich **Stacks** aus.

Am Ende der Übung haben Sie die zusätzliche Architektur erstellt, die in der folgenden Abbildung dargestellt ist. Die Tabelle hinter dem Diagramm enthält eine detaillierte Erläuterung der Architektur in Bezug auf die Aufgaben, die Sie in diesem Lab ausführen werden.

![end-arch](img/m4_a_02_end-arch.png)


| Numbered Task | Detail                                                                                                                                                                                                                                                                                         |
| ------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1             | Sie verwenden die Athena-Konfigurationsschritte und Abfragen, um eine AWS Glue-Datenbank mit Daten aus dem Taxi-Datensatz zu erstellen.                                                                                                                                                        |
| 2             | You will use buckets to analyze the taxi data for January. Note: The original dataset contains data for all months in the same file. Data for January has been split out of the original dataset into a separate file to optimize your Athena queries.                                         |
| 3             | Sie werden Athena verwenden, um Taxidaten zu analysieren, indem Sie Paytype verwenden, um die Daten zu partitionieren.                                                                                                                                                                         |
| 4             | Sie verwenden Athena Views, um den Gesamtwert der mit Kreditkarte oder Bargeld bezahlten Tarife zu berechnen.                                                                                                                                                                                  |
| 5             | Sie komprimieren die Datei mit den Dateitypen .zip und .gz und vergleichen die Größen der resultierenden Dateien.                                                                                                                                                                              |
| 6             | Sie werden die IAM-Richtlinie mit dem Titel Policy-for-Data-Scientists überprüfen.                                                                                                                                                                                                             |
| 7             | Mary hat die Berechtigungen aus dieser Richtlinie, weil sie zu der IAM-Gruppe gehört, der die Richtlinie zugewiesen ist. Sie werden diese Richtlinie verwenden, um ihren Zugriff auf den Datensatz und ihre Fähigkeit zu testen, die ursprünglichen Dateieigenschaften in Amazon S3 zu ändern. |
 
## Accessing the AWS Management Console

1. Wählen Sie oben in diesen Anweisungen Start Lab aus.
    - Die Laborsitzung beginnt.
    - Oben auf der Seite wird ein Timer angezeigt, der die verbleibende Zeit der Sitzung anzeigt.

!!! Tipp
    Um die Sitzungsdauer jederzeit zu aktualisieren, wählen Sie erneut Start Lab, bevor der Timer 00:00 Uhr erreicht.

   - Bevor Sie fortfahren, warten Sie, bis das Kreissymbol rechts neben dem AWS-Link in der oberen linken Ecke grün wird.

2. Um eine Verbindung zur AWS-Managementkonsole herzustellen, wählen Sie den AWS-Link in der oberen linken Ecke.
   - Ein neuer Browser-Tab wird geöffnet und verbindet Sie mit der Konsole.

!!! Tipp
    Wenn ein neuer Browser-Tab nicht geöffnet wird, befindet sich normalerweise oben in Ihrem Browser ein Banner oder Symbol mit der Meldung, dass Ihr Browser die Website daran hindert, Popup-Fenster zu öffnen. Wählen Sie das Banner oder Symbol aus und wählen Sie dann Popups zulassen aus.

## Task 1: Creating and querying an AWS Glue database and table in Athena

Ihre erste Aufgabe besteht darin, mithilfe von SQL-Anweisungen ein Schema für eine Tabelle zu definieren, das für die Arbeit mit den CSV-Beispieldaten verwendet werden kann.

In dieser Aufgabe werden Sie Folgendes tun:

- Geben Sie einen S3-Bucket für Abfrageergebnisse an.
- Erstellen Sie eine AWS Glue-Datenbank mithilfe des Athena-Abfrage-Editors.
- Erstellen Sie eine Tabelle in der AWS Glue-Datenbank und importieren Sie Daten.
- Sehen Sie sich eine Vorschau der Daten in der AWS Glue-Tabelle an.

## Task 2: Optimizing Athena queries by using buckets

Wenn Sie mit grossen Datensätzen arbeiten, die auf mehrere Dateien verteilt sind, sind zwei Hauptziele die Optimierung der Abfrageleistung und die Minimierung der Kosten. Die Kosten für Athena richten sich nach der Nutzung, d. h. der Menge der gescannten Daten, und die Preise variieren je nach Region.

Drei mögliche Strategien, mit denen Sie Ihre Kosten minimieren und die Leistung verbessern können, sind das Komprimieren von Daten, das Bucketisieren von Daten und das Partitionieren von Daten.

- **Daten komprimieren**: Komprimieren Sie Ihre Daten mithilfe eines der offenen Standards für die Dateikomprimierung (wie Gzip oder tar). Die Komprimierung führt zu einer kleineren Größe des Datensatzes, wenn er in Amazon S3 gespeichert wird.
- Die Kardinalität Ihrer Daten wirkt sich auch darauf aus, wie Sie Ihre Abfragen optimieren sollten. Weitere Informationen finden Sie unter Kardinalität (SQL-Anweisungen). Es gibt zwei Optionen für die Optimierung auf der Grundlage einer hohen oder niedrigen Kardinalität. Diese sind:
  - **Bucketisierung von Daten**: Um eine hohe Kardinalität mit Daten zu erreichen, speichern Sie Datensätze in unterschiedlichen Buckets (nicht zu verwechseln mit S3-Buckets), die auf einem gemeinsamen Wert in einem bestimmten Feld basieren. Erwägen Sie das Bucketisieren von Daten als Teil der Vorverarbeitungsphase Ihrer Datenpipeline. In diesem Lab werden die Daten für einen einzelnen Monat getrennt vom ursprünglichen Datensatz, der Daten für ein ganzes Jahr enthält, in Buckets unterteilt. Diese Strategie wird dazu beitragen, die Leistung zu optimieren.
  - **Partitionierung von Daten**: Sie können auch Partitionen verwenden, um die Leistung zu verbessern und die Kosten zu senken. Die Partitionierung wird bei Daten mit niedriger Kardinalität verwendet, was bedeutet, dass Felder nur wenige eindeutige oder unterschiedliche Werte haben.

In dieser Aufgabe experimentieren Sie mit der Bucketisierung von Daten, um Athena-Abfragen zu optimieren. Sie werden die folgenden Aktionen ausführen: 

- Erstellen Sie eine Tabelle mit dem Namen jan für Daten, die in Buckets unterteilt sind.
- Vergleichen Sie, wie lange es dauert, eine Abfrage für Bucketized-Daten für Januar 2017 auszuführen, und wie lange es dauert, den gesamten Datensatz nach den Daten vom Januar 2017 abzufragen.

## Task 3: Optimizing Athena queries by using partitions

Wenn Sie daran interessiert sind, ein Feld mit niedriger Kardinalität abzufragen, was bedeutet, dass das Feld nur wenige Einzelwerte enthält, würden Sie die Daten partitionieren, anstatt unterschiedliche Buckets zu verwenden. In einigen Fällen werden Ihre Daten durch einen anderen Prozess partitioniert. Um den effizientesten Ansatz zu finden, werden Sie versuchen, die Daten mithilfe des Paytype-Felds in einer bestimmten Abfrage in Athena zu partitionieren.

Das Paytype-Feld speichert die Art der Zahlung mithilfe der folgenden Codes:

- 1 = Kreditkarte
- 2 = Bargeld
- 3 = Keine Gebühr
- 4 = Rechtsstreit
- 5 = Unbekannt
- 6 = Stornierte Reise

Da die Menge der möglichen Werte begrenzt ist, eignen sich die Paytype_werte hervorragend für die Erstellung von Partitionen. In dieser Aufgabe verwenden Sie die Funktion CREATE TABLE AS, um die Daten zu partitionieren. Sie werden auch ein spaltenförmiges Speicherformat angeben.

Sie können Daten in Athena mit den Formaten Apache Parquet oder Optimized Row Columnar (ORC) speichern. Spaltenförmige Speicherformate komprimieren die Daten, wodurch die Kosten für Ihre Abfragen weiter gesenkt werden.

In dieser Aufgabe werden Sie mit der Verwendung von Partitionen für Ihre Abfragen experimentieren. Sie werden die folgenden Schritte ausführen:

- Erstellen Sie eine Tabelle, die auf dem Apache Parquet-Format basiert und das Paytype-Feld als Partition verwendet.
- Vergleichen Sie die Zeit, die für die Ausführung einer Abfrage in der gelben Datenbank nach Datensätzen mit dem Zahlungstyp 1 (Kreditkarte) benötigt wird, mit der Zeit, die für die Abfrage der partitionierten Tabelle benötigt wird, die mit dem Apache Parquet-Format erstellt wurde.

Zunächst erstellen Sie eine Partition, indem Sie eine Abfrage ausführen, um die Daten auszuwählen, die Sie für eine Partition verwenden möchten, und um ein Speicherformat anzugeben.

## Task 4: Using Athena views

Sie haben vielleicht bemerkt, dass Athena Ansichten (views) erstellen kann. Das Erstellen und Verwenden von Views mit Daten kann zur Vereinfachung der Analyse beitragen, da Sie einen Teil der Komplexität von Abfragen vor Benutzern verbergen können.

Athena unterstützt die Ausführung von jeweils nur einer SQL-Anweisung, aber Sie können Views verwenden, um Daten aus verschiedenen Tabellen zu kombinieren. Sie können Views auch verwenden, um die Abfrageleistung zu optimieren, indem Sie mit verschiedenen Methoden zum Abrufen von Daten experimentieren und dann die beste Abfrage als View speichern.

In dieser Aufgabe gehen Sie wie folgt vor: 

- Erstellen Sie eine View, um den Gesamtwert der Taxipreise in Dollar zu berechnen, die mit einer Kreditkarte bezahlt wurden.
- Erstellen Sie eine View, um den Gesamtwert von Fahrpreisen in Dollar zu berechnen, die bar bezahlt wurden.
- Rufen Sie alle Datensätze aus jeder dieser Views ab.
- Erstellen Sie eine neue View, die die Daten aus diesen beiden Views verbindet.
- Sehen Sie sich eine Vorschau der Ergebnisse der Verknüpfung an.

## Task 5: Creating Athena named queries by using CloudFormation

Das Data-Science-Team möchte die Abfragen teilen, die es mithilfe des Taxi-Datensatzes erstellt hat. Das Team würde sie gerne mit anderen Abteilungen teilen, aber diese Abteilungen verwenden andere Konten in AWS. Andere Abteilungen haben weniger Erfahrung mit AWS, daher möchte das Data-Science-Team den Prozess der Nutzung von Athena für andere Abteilungen vereinfachen.

!!! note
    In diesem Lab konzentrieren Sie sich auf die Erstellung einer CloudFormation-Vorlage zum Erstellen einer benannten Abfrage in Athena. Die Vorlage erstellt weder die AWS Glue-Datenbank noch die darin enthaltenen Tabellen und Daten. 

Sie möchten den Prozess zur Bereitstellung von Abfragen vereinfachen. Nach einigen Recherchen stellen Sie fest, dass die beste Lösung darin besteht, für jede Abfrage eine CloudFormation-Vorlage zu erstellen. Die Vorlage kann mit anderen Abteilungen geteilt und bei Bedarf bereitgestellt werden.

Zunächst möchten Sie experimentieren, indem Sie eine Beispielabfrage erstellen und sie mithilfe von Marys IAM-Benutzer testen. Wenn Sie erfolgreich sind, teilen Sie die Vorlage mit anderen Abteilungen.

## Task 6: Reviewing the IAM policy for Athena and AWS Glue access

Nachdem Sie die benannte Athena-Abfrage mithilfe von CloudFormation erstellt haben, überprüfen Sie die IAM-Richtlinie für die Abfrage, um sicherzustellen, dass andere sie in der Produktion verwenden können.

 Hinweis: Die IAM-Richtlinie für die Abfrage wurde für Sie erstellt. Sie sind nicht in der Lage, IAM-Richtlinien in der Laborumgebung zu erstellen.

## Task 7: Confirming that Mary can access and use the named query

Nachdem Sie die IAM-Richtlinie überprüft haben, werden Sie sie verwenden, um den Zugriff eines anderen Benutzers auf die benannte Abfrage und seine Fähigkeit, die Abfrage in der AWS-CLI zu verwenden, zu testen.

## Update from the team

## Submitting your work

## Lab complete


---
&copy;KSB, 2024, Modul 106, Datenbanken abfragen, bearbeiten und warten