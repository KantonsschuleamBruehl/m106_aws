# Ablauf und Lerninhalte des Fachgesprächs

![KSB Logo](img/KSB_logo.svg)

[TOC]

---

## Informationen/Tipps

Ihr Fachgespräch findet in Block 7 (26.06.) statt und zählt mit 40% zu Ihrer Note.

!!! info "Abgeschlossen"

    Die Ausarbeitung des Fragenkatalogs ist abgeschlossen. Es kommen keine weiteren Fragen hinzu.

!!! note "Tipp"

    Erarbeiten Sie sich vornezu die Antworten zum Fragenkatalog. So können Sie in der kurzen Zeit optimal performen und viele Fragen korrekt beantworten.

## Ablauf des Fachgesprächs

**Beurteilungsschema**

![](assets/2024-06-26-09-10-04-image.png){width="500"}

!!! info "Beurteilung"

    Im Beurteilungsschema können Sie erkennen, wie viele Fragen Sie aus den jeweiligen Lernfeldern bekommen und wie viele Punkte sie dafür erhalten.
    
    **Die Fragen werden Ihnen per Zufallsgenerator zugelost**
    
    
    Insgesamt haben Sie 10 Minuten Zeit um die 9 Fragen zu beantworten. Damit die Zeit ausreicht, ist es entscheidend, dass Sie sich entsprechend auf das Fachgespräch vorbereiten. 

### Einteilung Fachgespräch Mittwoch, 26.06.

![](assets/2024-06-26-09-05-56-image.png){width="500"}

---

&copy;KSB, 2024, Modul 106, Datenbanken abfragen, bearbeiten und warten