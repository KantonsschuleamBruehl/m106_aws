# Modul 11 Lab Analyzing and Visualizing Streaming Data with Kinesis Data Firehose, OpenSearch Service, and OpenSearch Dashboards

![KSB Logo](img/KSB_logo.svg)

[TOC]

---

## Lab overview and objectives

Big-Data-Probleme erfordern oft Lösungen in Echtzeit. Dies ist der Geschwindigkeitsteil der fünf V's von Big Data (volume, variety, velocity, veracity, and value (Volumen, Vielfalt, Geschwindigkeit, Richtigkeit und Wert)). Zu den üblichen Datenquellen für diese Szenarien gehören Videostreams, Anwendungsprotokolle und Infrastrukturgeräte. Daten in diesen Geschwindigkeitsszenarien werden als Streaming-Daten bezeichnet. Amazon Kinesis ist eine Suite von Diensten, mit denen Sie Streaming-Daten analysieren können.

In diesem Lab verwenden Sie Amazon Kinesis Data Streams, um Daten von einem Webserver zu sammeln, der in Amazon Elastic Compute Cloud (Amazon EC2) gehostet wird. Anschliessend verwenden Sie AWS Lambda, um die Daten zu verarbeiten und anzureichern. Sie analysieren die Daten mithilfe von OpenSearch Dashboards, wo Sie einen Index erstellen und die Daten dann visualisieren können, um Dashboards zu erstellen. Geschäftsanwender können mithilfe von Amazon Cognito auf Dashboards zugreifen.

Nach Abschluss dieses Labors sollten Sie in der Lage sein, Folgendes zu tun:

* Die Laborinfrastruktur in der AWS-Managementkonsole beschreiben.
* Webserver-Protokolle in Amazon Kinesis Data Firehose und Amazon OpenSearch Service erfassen.
* Den Datenaufnahme- und Transformationsprozess mithilfe von Amazon CloudWatch-Protokollereignissen beobachten.
* Einen Index für diese Protokolle in OpenSearch Service erstellen.
* Daten mithilfe von OpenSearch-Dashboards visualisieren, einschliesslich:
  * ein Kreisdiagramm, das die Betriebssysteme und Browser veranschaulicht, mit denen Besucher die Website nutzen, erstellen.
  * Eine Heatmap, die veranschaulicht, wie Benutzer auf Produktseiten weitergeleitet werden (entweder über die Suchseite oder die Empfehlungsseite), erstellen.

## Scenario

Die verantwortliche Administratorin der Website der Universitätsbuchhandlung möchte Erkenntnisse darüber sammeln, wie Besucher mit der Website interagieren. Sie hat ein webbasiertes JavaScript-Tracking-System verwendet, das Diagramme mit Informationen zur Benutzeraktivität enthält. Zu den Informationen gehört, wo sich Benutzer befinden, welche Browser sie verwenden und ob sie mobile Geräte verwenden. Die Daten können auch Aufschluss darüber geben, ob Besucher die Seite eines Buchhandlungsprodukts von einer Suchseite oder einer Empfehlungsseite aus erreicht haben.

Die Anzahl der Besucher der Website ist von Jahr zu Jahr gestiegen. Da jedoch viele Besucher Browser verwenden, die Tracking-Skripte von Drittanbietern blockieren, ist sie jetzt besorgt, dass die Daten über die Benutzeraktivitäten nicht korrekt sind.

Bei der Recherche, wie genauere Daten abgerufen werden können, stiess sie auf den Beitrag [„Benutzerverhalten mit Amazon Elasticsearch Service, Amazon Kinesis Data Firehose und Kibana analysieren“](https://aws.amazon.com/blogs/database/analyze-user-behavior-using-amazon-elasticsearch-service-amazon-kinesis-data-firehose-and-kibana/) im AWS-Datenbank-Blog. Der Beitrag beschreibt eine Lösung zur Verwendung von Streaming-Daten zur Analyse der Protokolle für einen Webserver und zur Bestimmung von Benutzerzugriffsmustern. Der Webadministratorin hat erfahren, dass Ihr Team AWS-Services zur Erstellung von Datenanalyselösungen verwendet. Sie bittet Sie um Rat, ob der Ansatz aus dem Blogbeitrag dieselben Informationen wie ihr aktuelles Tracking-System liefern könnte.

Der Blogbeitrag enthält Dateien für eine einfache Website mit einer Suchseite und einer Empfehlungsseite, die Besucher auf einige Produktseiten weiterleiten. Sie beschliessen, diese einfache Website zu verwenden, um einen Machbarkeitsnachweis (POC) zu entwickeln, mit dem Streaming-Daten die Benutzeraktivitäten für die Website der Buchhandlung analysiert werden.

Die Lösung verwendet Kinesis Data Firehose, um gestreamte Webserver-Zugriffsprotokolle zu erfassen, und verwendet dann Lambda-Funktionen, um die Daten anzureichern. Nachdem Sie Kinesis und Lambda zur Verarbeitung der Daten verwendet haben, können Sie OpenSearch Service verwenden, um die Daten zu laden und zu indizieren. Mithilfe von OpenSearch Dashboards können Sie Visualisierungen der Daten erstellen, um Einblicke in die Besucher der Websites der Universität zu erhalten. Sie können diese Visualisierungen teilen, indem Sie Amazon Cognito als Authentifizierungstool und AWS Identity and Access Management (IAM) verwenden, um den Zugriff auf das Dashboard zu autorisieren.

Wenn Sie das Lab starten, enthält die Umgebung die Ressourcen, die in der folgenden Abbildung dargestellt sind.

![M11 Enviroment Start](img/m11_01_enviroment_start.png)

Die Infrastruktur ist für die Analyse von Streaming-Daten konzipiert und besteht aus den folgenden Komponenten:

* Eine EC2-Instanz, auf der ein Webserver ausgeführt wird. Die Instance hat ein öffentliches Subnetz.
  Der Webserver, der auf der Instanz ausgeführt wird, enthält eine Website mit den folgenden Dateien. Wenn Sie das Website-Paket überprüfen möchten, können Sie diese ZIP-Datei herunterladen und öffnen:
    * httpd.conf: Konfigurationsdatei für den Apache-Webserver
    * agent.json: Konfigurationsdatei zur Verbindung des Webservers mit einem Kinesis Data Firehose-Lieferstream
    * search.php: Seite, auf der ein Benutzer nach einem Produkt suchen kann
    * recommendation.php: Seite, die ein bestimmtes Produkt auf der Grundlage des Suchverlaufs des Benutzers empfiehlt
    * echo.php, kindle.php und firetvstick.php: Seiten für drei Produkte auf der Website
* Ein Kinesis Data Firehose-Lieferstream, der Streaming-Daten aus den Webserver-Protokollen erfasst. Sie verwenden den Lieferstream, um Daten in einen OpenSearch Service-Cluster zu schreiben.
* Eine Lambda-Funktion zur Transformation der Daten. Wenn Sie die Funktion überprüfen möchten, können Sie diese ZIP-Datei herunterladen und öffnen.
* Ein OpenSearch Service-Cluster zum Indizieren und Speichern der Daten.
* Eine OpenSearch Dashboards-Instanz zum Erstellen von Datenvisualisierungen und zum Gewinnen von Erkenntnissen aus den Daten.

Am Ende des Labors werden Sie die Architektur zur Ausführung mehrerer Aufgaben verwendet haben. Die Tabelle hinter dem Diagramm enthält eine detaillierte Erläuterung dieser Aufgaben in Bezug auf die Laborarchitektur.

![M11 Lab Enviroment End](img/m11_02_enviroment_end.png)

| Numbered Step | Detail                                                                                                                                                                                                                                                                                              |
| ------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1             | Sie überprüfen die EC2-Instance-Konfiguration für den Webserver. Sie werden auch die IAM-Rolle OSDemoWebServerIAMRole und die OSDemoWebServerIAMPolicy-IAM-Richtlinien überprüfen, um zu verstehen, welche IAM-Berechtigungen auf die Rolle angewendet werden.                                      |
| 2             | Sie überprüfen den Kinesis-Datenstrom, der so konfiguriert ist, dass er die Webzugriffsprotokolle für Benutzer erfasst, die auf die Website zugreifen.                                                                                                                                              |
| 3             | Sie werden auch die Konfiguration für den OpenSearch Service-Cluster überprüfen, der im Labor verwendet wird.                                                                                                                                                                                       |
| 4             | Anschliessend richten Sie den OpenSearch Service-Index ein.                                                                                                                                                                                                                                         |
| 5             | Nachdem Sie alles konfiguriert haben, durchsuchen Sie die Website, um Zugriffsprotokolle zu generieren.                                                                                                                                                                                             |
| 6             | Nachdem Sie Zugriffsprotokolle generiert haben, werden die CloudWatch-Protokollereignisse auch im AWS-Konto generiert. Sie werden diese Protokolle überprüfen, um besser zu verstehen, wie Kinesis Data Firehose diese aufnimmt und zur weiteren Anreicherung an eine Lambda-Funktion weiterleitet. |
| 7             | Sie erstellen ein OpenSearch Service-Indexmuster, das für die Erstellung von Visualisierungen in OpenSearch Dashboards benötigt wird.                                                                                                                                                               |
| 8             | Sie werden eine Stückdiagrammvisualisierung in OpenSearch Dashboards erstellen.                                                                                                                                                                                                                     |
| 9             | Zum Abschluss des Labors erstellen Sie eine Heatmap-Visualisierung.                                                                                                                                                                                                                                 |

---
&copy;KSB, 2024, Modul 106, Datenbanken abfragen, bearbeiten und warten