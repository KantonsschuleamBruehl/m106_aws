![ksb_logo_klein.jpg](img/ksb_logo_klein.jpg)

---

# M106 Datenbanken abfragen, bearbeiten und warten

Herzlich Willkommen auf den Doku-Seiten zum Modul

==Viel Erfolg im Modul!==

![](img/aws_pic.png)

## Data Management

!!! Info "Kompetenz"

    Bereitet Daten durch Abfragen auf und nimmt Optimierungen zur Leistungssteigerung vor. Ändert Struktur und Daten einer Datenbank, schützt die Daten durch Zugriffsberechtigungen und sichert die Daten wie auch das Datenbankschema in einem Backup.

!!! Info "Objekt"
    Datenbanken mit bis zu zehn Tabellen (einfache, komplexe und rekursive Beziehungstypen) und schützenswerten Daten (z.B. Kunden- oder Patientenverwaltung).

## Handlungsziele und handlungsnotwendige Kenntnisse

??? success "Erarbeitet ein Datensicherheits- und Rollenkonzept und dokumentiert dieses."

    1. Kennt Befehle einer Datenkontrollsprache (DCL) um Benutzer und/oder Rollen zu verwalten.
    
    2. Kennt Befehle einer Datenkontrollsprache (DCL), um Zugriffsberechtigungen an Benutzer und/oder Rollen zuzuweisen und zu verwalten.

??? success "Führt Befehle zur Abfrage der Daten aus und nutzt Filter- sowie Aggregationsfunktionen."

    1. Kennt die Notwendigkeit der Sicherung von verbundenen Aktionsschritten durch Transaktionen.
    
    2. Kennt die Kriterien des ACID-Prinzips (Atomic, Constistent, Isolated, Durable), worüber Transaktionen charakterisiert werden.
    
    3. Kennt Befehle einer Transaktionskontrollsprache (TCL) zur Definition von Transaktionsgrenzen und Sicherungspunkten in Manipulationsschritten von über mehreren Tabellen verteilten, einzufügenden oder zu ändernden Datensätzen.

??? success "Sichert Daten und Datenbankschema in einem Backup und stellt daraus die Daten sowie das Datenbankschema wieder her."

    1. Kennt Befehle eines Datenbankmanagementsystems (DBMS) zur Sicherung einer Datenbank (Daten und Datenbankschema) im ruhenden und laufenden Betrieb.
    
    2. Kennt Befehle eines Datenbankmanagementsystems (DBMS) zur Wiederherstellung einer Datenbank (Daten und Datenbankschema) oder Teile davon.

??? success "Ändert oder migriert ein Datenbankschema und die Daten einer Datenbank."

    1. Kennt Befehle einer Datendefinitionssprache (DDL) zur Anpassung eines physischen Datenbankschemas und allenfalls notwendige Massnahmen zur Sicherstellung der Datenintegrität und -vollständigkeit.

??? success "Optimiert die Datenbank bezüglich Zugriffszeiten und Ressourcenbedarf."

    1. Kennt Befehle einer Datendefinitionssprache (DDL) zum Ergänzen von Indizes zur Beschleunigung von Abfragen auf bestimmten Feldern.

---
&copy;KSB, 2024, Modul 106, Datenbanken abfragen, bearbeiten und warten